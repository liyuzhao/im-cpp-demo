//
//  empresencemanager_interface.h
//  easemob
//
//  Created by lixiaoming on 2022/1/5.
//

#ifndef EMCORE_PRESENCEMANAGER_INTERFACE_H
#define EMCORE_PRESENCEMANAGER_INTERFACE_H

#include <vector>
#include <string>
#include "empresence.h"
#include "emerror.h"
#include "empresencemanager_listener.h"
#include "emdefines.h"


namespace easemob
{
class EASEMOB_API EMPresenceManagerInterface {

public:
    virtual ~EMPresenceManagerInterface() {};
    /**
     * \brief Publish a custom presence status
     *
     * @param  presenceValue EMPresence::Value The custom presence value
     * @return EMErrorPtr The result of operation
     */
    virtual EMErrorPtr publishPresence(const int& presenceStauts ,const std::string& ext = "") = 0;
    /**
     * \brief Subscribe some uids's presence status
     *
     * @param  members stringArray  The uids to subscribe
     * @param  outputPresences ObjectArray The current presences of members
     * @param  expiry The expity seconds of subscribetion
     * @return EMErrorPtr The result of operation
     */
    virtual EMErrorPtr subscribePresences(const std::vector<std::string>& members,std::vector<EMPresencePtr>& outputPresences,long long expiry) = 0;
    /**
     * \brief Unsubscribe some uids's presence status
     *
     * @param  members stringArray  The uids to unsubscribe
     * @return EMErrorPtr The result of operation
     */
    virtual EMErrorPtr unsubscribePresences(const std::vector<std::string>& members) = 0;
    /**
     * \brief Fetch  uids which user have subscribed
     *
     * @param  members  stringArray output.  The uids user have subscribed.
     * @return EMErrorPtr The result of operation
     */
    virtual EMErrorPtr fetchSubscribedMembers(std::vector<std::string>& members,uint32_t pageNum,uint32_t pageSize) = 0;
    /**
     * \brief Fetch  some presence status of members
     *
     * @param  members  stringArray input.  The uids to fetch presence
     * @param  presences output. The presences result
     * @return EMErrorPtr the result of operation
     */
    virtual EMErrorPtr fetchPresenceStatus(const std::vector<std::string>& members,std::vector<EMPresencePtr>& presences) = 0;
    /**
     * \brief Add a listener to presence manager
     *
     * @param  presenceManagerListerner EMPresenceManagerListener
     * @return NA
     */
    virtual void addListener(EMPresenceManagerListener*presenceManagerListerner) = 0;
    /**
     * \brief Remove a listener from presence manager
     *
     * @param  presenceManagerListerner EMPresenceManagerListener
     * @return NA
     */
    virtual void removeListener(EMPresenceManagerListener*presenceManagerListerner) = 0;
    /**
     * \brief Remove all listeners from presence manager
     *
     * @return NA
     */
    virtual void clearListeners() = 0;
};
}

#endif
