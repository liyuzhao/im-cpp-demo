//
//  empresencemanager_listener.h
//  easemob
//
//  Created by lixiaoming on 2022/1/5.
//

#ifndef EMCORE_PRESENCEMANAGER_LISTENER_H
#define EMCORE_PRESENCEMANAGER_LISTENER_H
#include "empresence.h"

namespace easemob
{
    class EMPresenceManagerListener {
    public:
        virtual ~EMPresenceManagerListener(){}
        virtual void onPresenceUpdated(const std::vector<EMPresencePtr>& presence) = 0;
    };
}

#endif
