//
//  empresence.h
//  easemob
//
//  Created by lixiaoming on 2022/1/5.
//

#ifndef empresence_h
#define empresence_h

#include <stdio.h>
#include <string>
#include <set>
#include <memory>

namespace easemob
{
class EMPresence {
public:
    virtual ~EMPresence();
    EMPresence(const std::string& publisher = std::string(""));
    void setPublisher(const std::string& publisher) {mPublisher = publisher;}
    std::string getPublisher(){return mPublisher;}
    void setStatusList(const std::set<std::pair<std::string,int>>& devices) {mStatusList = devices;}
    std::set<std::pair<std::string,int>> getStatusList(){return mStatusList;}
    void setExt(const std::string& ext){mExt = ext;}
    std::string getExt(){return mExt;}
    void setLatestTime(const int64_t& latestTime) {mLatestTime = latestTime;}
    int64_t getLatestTime(){return mLatestTime;}
    void setExpiryTime(const int64_t& expiryTime) {mExpiryTime = expiryTime;}
    int64_t getExpiryTime(){return mExpiryTime;}
private:
    std::string mPublisher; //Presence发布者Id
    std::set<std::pair<std::string,int>> mStatusList; //各设备的presence状态
    std::string mExt; //扩展字段
    int64_t mLatestTime; //更新时间
    int64_t mExpiryTime; //订阅到期时间
};
typedef std::shared_ptr<EMPresence> EMPresencePtr;
}

#endif /* empresence_h */
