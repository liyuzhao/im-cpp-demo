/************************************************************
 *  * EaseMob CONFIDENTIAL
 * __________________
 * Copyright (C) 2015 EaseMob Technologies. All rights reserved.
 * 
 * NOTICE: All information contained herein is, and remains
 * the property of EaseMob Technologies.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from EaseMob Technologies.
 */
//
//  EMTextMessageBody.h
//
//  Copyright (c) 2015 EaseMob Inc. All rights reserved.
//

#ifndef __easemob__EMTextMessageBody__
#define __easemob__EMTextMessageBody__

#include <string>
#include <vector>
#include <map>
#include "emmessagebody.h"

namespace easemob {

class EASEMOB_API EMTextMessageBody : public EMMessageBody
{
public:
    /**
      * \brief Text message body constructor.
      *
      * @param  The text.
      * @return NA
      */
    EMTextMessageBody(const std::string& = "");
    
    /**
      * \brief Class destructor.
      *
      * @param  NA
      * @return NA
      */
    virtual ~EMTextMessageBody();

    /**
     * \brief Set the text.
     *
     * @param  The text.
     * @return NA
     */
    void setText(const std::string &);
    
    /**
      * \brief Get the text.
      *
      * @param  NA
      * @return The text.
      */
    const std::string& text() const;
    /**
     * \brief Set message target languages.
     *
     * @Param languages message target languages.
     */
    void setTargetLanguages(const std::vector<std::string>& languages) {
        mTargetLanguages = languages;
    }
    
    /**
     * \brief Get message target language list.
     *
     * @param  NA
     * @return The message target language list.
     */
    std::vector<std::string> getTargetLanguages() const {return mTargetLanguages;}
    
    /**
     * \brief Set message translations.
     *
     * @Param translations message translations.
     */
    void setTranslations(const std::map<std::string,std::string>& translations) {
        mTranslations = translations;
    }
    
    /**
     * \brief Get message translation list.
     *
     * @param  NA
     * @return The message translation list.
     */
    std::map<std::string, std::string> getTranslations() const {
        return mTranslations;
    }
    
private:
    EMTextMessageBody(const EMTextMessageBody&);
    EMTextMessageBody& operator=(const EMTextMessageBody&);
    virtual void dummy() const{}
    std::string mText;
    std::vector<std::string> mTargetLanguages;
    std::map<std::string, std::string> mTranslations;
    friend class EMTextMessageBodyPrivate;
};
    
typedef std::shared_ptr<EMTextMessageBody> EMTextMessageBodyPtr;

}
#endif /* defined(__easemob__EMTextMessageBody__) */
