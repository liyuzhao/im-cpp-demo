# 基于环信SDK(C++版)的简单Demo及文档
---
> 本项目致力于基于C++开发桌面端或嵌入式系统的用户提供能力。


[进入文档目录](docs/README.md)

操作系统支持情况：

- Windows(x86_64): **已支持**

- MacOS(amd64): **已支持**

- Linux(x86_64): **已支持**

- Linux(arm64): **已支持**


**注意**: 

Linux版本默认使用`openssl_1.1.x`，如系统版本比较老，必须使用`openssl_1.0.x`，只需要将`bak`文件夹下的对应的库替换`sdk`内的so即可。



**环信SDK版本**：**SDK_3.9.1**  (如需更多版本，请在issue上提出)

Demo支持的IDE:

- XCode
- Clion
- Visual Studio 2019
- QtCreator
- Visual Studio Code

---

Demo的运行步骤：

```
git clone ...
mkdir build
cd build
cmake ..
make
```





