//
// Created by liyuzhao on 12/15/22.
//

#include "metadatademo.h"
#include <emchatconfigs.h>
#include <emuserinfomanager_interface.h>

using namespace std;
using namespace easemob;

MetaDataDemo::MetaDataDemo() : BaseConfig() {

}

void MetaDataDemo::testMetaData() {

//    createTestAccount();

    const std::string &loginUserName = TestAccounts[0];
    easemob::EMErrorPtr retPtr = mClient->login(loginUserName, "123456");
    if (retPtr->mErrorCode != EMError::EM_NO_ERROR)
    {
        std::cout << "login failed!" << retPtr->mErrorCode << std::endl;
        return;
    }
    std::cout << "login success!" << std::endl;

    EMError error;
    // 设置用户属性
    std::string response;

    std::string strUserProperty = "{\"nickname\":\"王者荣耀\", \"avatarurl\":\"http://www.easemob.com/avatar.png\", \"ext\":\"ext\"}";
    mClient->getUserInfoManager().updateOwnUserInfo(strUserProperty, response, error);

    if(error.mErrorCode != EMError::EM_NO_ERROR) {
        std::cerr << "updateOwnUserInfo failed, cause by: " << error.mDescription << std::endl;
        return;
    }

    std::cout << "response=>" << response << std::endl;

    strUserProperty = "";
    mClient->getUserInfoManager().fetchUsersPropertyByType({TestAccounts[0], TestAccounts[1], TestAccounts[2]}, {"avatarurl", "nickname"}, strUserProperty, error);
    if(error.mErrorCode != EMError::EM_NO_ERROR) {
        std::cerr << "fetchUsersPropertyByType failed, cause by: " << error.mDescription << std::endl;
        return;
    }


    std::cout << "strUserProperty=>" << strUserProperty << std::endl;




}