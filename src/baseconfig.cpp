//
// Created by liyuzhao on 2023/9/11.
//

#include "baseconfig.h"
#include <emchatconfigs.h>
#include <emlogininfo.h>
#include <emerror.h>

BaseConfig::BaseConfig(){
    TestAccounts = {"ceshia", "ceshib", "ceshic", "ceshid"};
    initPrivate();
}


void BaseConfig::initPrivate() {

    std::string appkey = "easemob-demo#zim";

    // 配置参数：1. 资源路径(下载的文件等)  2. 工作路径(日志、配置信息等)  3. appkey
    easemob::EMChatConfigsPtr configs(new easemob::EMChatConfigs("./resourcePath", "./workPath", appkey));
    easemob::EMChatConfigsPtr configPtr = easemob::EMChatConfigsPtr(configs);

    // 配置Resources: 移动端、Windows、Mac、Linux等
    configs->setOs(easemob::EMChatConfigs::OS_MSWIN);
    configs->setClientResource("win"); // resource 名称
    configPtr->setEnableConsoleLog(true); // 打印控制台日志
    configPtr->setAutoAcceptGroup(true); // 是否自动接受群邀请

    // 设置私有部署环境
    auto priConfig = configPtr->privateConfigs();
    priConfig->enableDnsConfig(false);
//    priConfig->chatServer() = "172.17.6.204";
//    priConfig->chatPort() = 6717;
//    priConfig->restServer() = "http://172.17.6.204:12000";

    priConfig->chatServer() = "180.184.183.69";
    priConfig->chatPort() = 6717;
    priConfig->restServer() = "http://180.184.183.69:12000";

    mClient = easemob::EMClient::create(configPtr);

}

void BaseConfig::createTestAccount() {

    auto creatAccounts = TestAccounts;

    std::string defaultAccountPwd = "123456";

    easemob::EMErrorPtr errorPtr;
    for(const std::string & account : creatAccounts) {
        errorPtr = mClient->createAccount(account, defaultAccountPwd);
        if(errorPtr->mErrorCode == easemob::EMError::EM_NO_ERROR) {
            std::cout << "create account :" << account << " success！" << std::endl;
        } else {
            std::cout << "create account :" << account <<  " failed, cause by：" << errorPtr->mDescription << std::endl;
        }
    }


}