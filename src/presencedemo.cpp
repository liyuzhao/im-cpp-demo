//
// Created by liyuzhao on 12/20/22.
//

#include "presencedemo.h"
#include <iostream>
#include <empresencemanager_interface.h>
#include <empresencemanager_listener.h>
#include <thread>
#include <functional>
#include <memory>
#include <chrono>

using namespace std;
using namespace easemob;

void printPresences(const std::vector<EMPresencePtr> &presences);


PresenceDemo::PresenceDemo():BaseConfig() {
    std::cout << "== PresenceDemo constructor ==" << std::endl;
}


class PreferenceListener : public EMPresenceManagerListener
{
    void onPresenceUpdated(const std::vector<EMPresencePtr>& presence);
};

void PreferenceListener::onPresenceUpdated(const std::vector<EMPresencePtr> &presence) {
    std::cout << "notify-onPresenceUpdated" << std::endl;
    printPresences(presence);
}


void printPresences(const std::vector<EMPresencePtr> &presences)
{
    std::cout << "presences==>" << std::endl;
    for (auto& presence : presences) {
        std::cout << "uid:" << presence->getPublisher() << ", ext:" << presence->getExt() << ", devices:" << std::endl;
        using DeviceStatus = std::pair<std::string, int>;
        auto statusList = presence->getStatusList();
        for (const DeviceStatus& item : statusList) {
            std::cout << item.first << ":" << item.second << std::endl;
        }
    }

    std::cout << std::endl;
}


void PresenceDemo::testPresence() {

    const std::string &loginUserName = TestAccounts[1];

    auto listener = std::unique_ptr<PreferenceListener>(new PreferenceListener());
    mClient->getPresenceManager().addListener(listener.get());

    easemob::EMErrorPtr retPtr = mClient->login(loginUserName, "123456");
    if (retPtr->mErrorCode != EMError::EM_NO_ERROR)
    {
        std::cout << "login failed!" << retPtr->mErrorCode << std::endl;
        return;
    }
    std::cout << "login success!" << std::endl;

    retPtr = mClient->getPresenceManager().publishPresence(1, "busy");

    if(retPtr->mErrorCode != EMError::EM_NO_ERROR) {
        std::cout << "publishPresence failed! error=>" << retPtr->mDescription  << std::endl;
        return;
    }

    std::vector<EMPresencePtr> statusRet; // 返回的在线状态列表

    // 订阅指定用户的在线状态
    retPtr = mClient->getPresenceManager().subscribePresences({TestAccounts[0], TestAccounts[2]}, statusRet, 3000);
    if(retPtr->mErrorCode != EMError::EM_NO_ERROR) {
        std::cout << "subscribePresences failed! error=>" << retPtr->mDescription  << std::endl;
        return;
    }

    std::vector<std::string> members;
    int pageNum = 0;
    int pageSize = 200;
    retPtr = mClient->getPresenceManager().fetchSubscribedMembers(members, pageNum, pageSize);
    if(retPtr->mErrorCode != EMError::EM_NO_ERROR) {
        std::cout << "fetchSubscribedMembers failed! error=>" << retPtr->mDescription  << std::endl;
        return;
    }

    retPtr = mClient->getPresenceManager().unsubscribePresences(members);
    if(retPtr->mErrorCode != EMError::EM_NO_ERROR) {
        std::cout << "unsubscribePresences failed! error=>" << retPtr->mDescription  << std::endl;
        return;
    }


    retPtr = mClient->getPresenceManager().fetchPresenceStatus({TestAccounts[0], TestAccounts[1]}, statusRet);
    if(retPtr->mErrorCode != EMError::EM_NO_ERROR) {
        std::cout << "fetchPresenceStatus failed! error=>" << retPtr->mDescription  << std::endl;
        return;
    }

    printPresences(statusRet);

    std::this_thread::sleep_for(std::chrono::seconds(15));




}

