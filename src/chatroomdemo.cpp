//
// Created by liyuzhao on 11/9/22.
//

#include "chatroomdemo.h"
#include <iostream>
#include <emchatconfigs.h>
#include <emclient.h>
#include <emchatroommanager_interface.h>
#include <emchatroommanager_listener.h>
#include <vector>
#include <algorithm>

using namespace std;
using namespace easemob;

ChatRoomDemo::ChatRoomDemo():BaseConfig(){
    std::cout << "======= ChatRoomDemo construction =========" << std::endl;
}

class ChatRoomListener : public EMChatroomManagerListener {
public:
    ChatRoomListener(){}
    void onLeaveChatroom(const EMChatroomPtr chatroom, EMMuc::EMMucLeaveReason reason) {}
    void onMemberJoinedChatroom(const EMChatroomPtr chatroom, const std::string &member) {};
    void onMemberLeftChatroom(const EMChatroomPtr chatroom, const std::string &member) {};
    void onAddMutesFromChatroom(const EMChatroomPtr chatroom, const std::vector<std::string> &mutes, int64_t muteExpire) {};
    void onRemoveMutesFromChatroom(const EMChatroomPtr chatroom, const std::vector<std::string> &mutes) {};
    void onAddWhiteListMembersFromChatroom(const easemob::EMChatroomPtr chatroom, const std::vector<std::string> &members) {};
    void onRemoveWhiteListMembersFromChatroom(const easemob::EMChatroomPtr chatroom, const std::vector<std::string> &members) {};
    void onAllMemberMuteChangedFromChatroom(const easemob::EMChatroomPtr chatroom, bool isAllMuted) {};
    void onAddAdminFromChatroom(const EMChatroomPtr chatroom, const std::string &admin) {};
    void onRemoveAdminFromChatroom(const EMChatroomPtr chatroom, const std::string &admin) {};
    void onAssignOwnerFromChatroom(const EMChatroomPtr chatroom, const std::string &newOwner, const std::string &oldOwner) {};
    void onUpdateAnnouncementFromChatroom(const EMChatroomPtr chatroom, const std::string &announcement) {};

};



void ChatRoomDemo::testChatRoom(){
    const std::string &loginUserName = TestAccounts[3];
    easemob::EMErrorPtr retPtr = mClient->login(loginUserName, "123456");
    if (retPtr->mErrorCode != EMError::EM_NO_ERROR)
    {
        std::cerr << "login failed!" << retPtr->mErrorCode << std::endl;
        return;
    }
    std::cout << "login success!" << std::endl;

    ChatRoomListener * listener = new ChatRoomListener();
    mClient->getChatroomManager().addListener(listener);


    EMError error;
    int maxUserCount = 200;
    bool inviteNeedConfirm = false;
    // 登录用户必须是聊天室的超级管理员才行
    // http://docs-im-beta.easemob.com/document/server-side/chatroom.html#%E6%B7%BB%E5%8A%A0%E8%B6%85%E7%BA%A7%E7%AE%A1%E7%90%86%E5%91%98
    // # 将 <YourAppToken> 替换为你在服务端生成的 App Token
    // curl -X POST -H 'Authorization: Bearer <YourAppToken>' -H 'Content-Type: application/json' 'http://172.17.6.204:12000/easemob-demo/zim/chatrooms/super_admin' -d '{"superadmin", "ceshid"}'
    auto chatRoomPtr = mClient->getChatroomManager().createChatroom("chatRoomName", "chatRoomDesc", "welcomeMessage", EMMucSetting(EMMucSetting::EMMucStyle::PUBLIC_JOIN_OPEN, maxUserCount, inviteNeedConfirm), { TestAccounts[0], TestAccounts[1], TestAccounts[2]}, error);
    if(error.mErrorCode != EMError::EM_NO_ERROR) {
        std::cerr << "create ChatRoom failed, cause by: " << error.mDescription << std::endl;
        return;
    }


    auto chatRoomList = mClient->getChatroomManager().fetchAllChatrooms(error);

    if(error.mErrorCode != EMError::EM_NO_ERROR) {
        std::cerr << "fetchAllChatrooms failed, cause by: " << error.mDescription << std::endl;
        return;
    }

    mClient->getChatroomManager().joinChatroom(chatRoomPtr->chatroomId(), error);
    if(error.mErrorCode != EMError::EM_NO_ERROR) {
        std::cerr << "joinChatroom failed, cause by: " << error.mDescription << std::endl;
        return;
    }
    std::cout << "joinChatroom success" << std::endl;

    mClient->getChatroomManager().leaveChatroom(chatRoomPtr->chatroomId(), error);
    if(error.mErrorCode != EMError::EM_NO_ERROR) {
        std::cerr << "leaveChatroom failed, cause by: " << error.mDescription << std::endl;
        return;
    }
    std::cout << "leaveChatroom success" << std::endl;



    std::cout << "fetch chatroom size :" << chatRoomList.size() << std::endl;


    auto chatRoomS = mClient->getChatroomManager().fetchChatroomSpecification(chatRoomPtr->chatroomId(), error);


    std::string cursor;
    int pageSize = 200;
    auto cursorRet = mClient->getChatroomManager().fetchChatroomMembers(chatRoomPtr->chatroomId(), cursor, pageSize, error);
    if(error.mErrorCode != EMError::EM_NO_ERROR) {
        std::cerr << "fetchChatroomMembers failed, cause by: " << error.mDescription << std::endl;
        return;
    }
    for(auto & s : cursorRet.result()) {
        std::cout << "member =>" << s << std::endl;
    }

    mClient->getChatroomManager().removeChatroomMembers(chatRoomPtr->chatroomId(), {TestAccounts[0]}, error);
    if(error.mErrorCode != EMError::EM_NO_ERROR) {
        std::cerr << "fetchChatroomMembers failed, cause by: " << error.mDescription << std::endl;
        return;
    }


    mClient->getChatroomManager().blockChatroomMembers(chatRoomPtr->chatroomId(), {TestAccounts[2]}, error);
    if(error.mErrorCode != EMError::EM_NO_ERROR) {
        std::cerr << "blockChatroomMembers failed, cause by: " << error.mDescription << std::endl;
        return;
    }

    mClient->getChatroomManager().fetchChatroomBans(chatRoomPtr->chatroomId(), 0, pageSize, error);
    if(error.mErrorCode != EMError::EM_NO_ERROR) {
        std::cerr << "fetchChatroomBans failed, cause by: " << error.mDescription << std::endl;
        return;
    }

    mClient->getChatroomManager().unblockChatroomMembers(chatRoomPtr->chatroomId(), {TestAccounts[2]}, error);
    if(error.mErrorCode != EMError::EM_NO_ERROR) {
        std::cerr << "unblockChatroomMembers failed, cause by: " << error.mDescription << std::endl;
        return;
    }


    mClient->getChatroomManager().muteChatroomMembers(chatRoomPtr->chatroomId(), {TestAccounts[1]}, 60 * 1000, error);

    if(error.mErrorCode != EMError::EM_NO_ERROR) {
        std::cerr << "muteChatroomMembers failed, cause by: " << error.mDescription << std::endl;
        return;
    }

    mClient->getChatroomManager().fetchChatroomMutes(chatRoomPtr->chatroomId(), 0, pageSize, error);
    if(error.mErrorCode != EMError::EM_NO_ERROR) {
        std::cerr << "fetchChatroomMutes failed, cause by: " << error.mDescription << std::endl;
        return;
    }


    mClient->getChatroomManager().unmuteChatroomMembers(chatRoomPtr->chatroomId(), {TestAccounts[1]}, error);

    if(error.mErrorCode != EMError::EM_NO_ERROR) {
        std::cerr << "unmuteChatroomMembers failed, cause by: " << error.mDescription << std::endl;
        return;
    }

    mClient->getChatroomManager().addChatroomAdmin(chatRoomPtr->chatroomId(), TestAccounts[1], error);
    if(error.mErrorCode != EMError::EM_NO_ERROR) {
        std::cerr << "addChatroomAdmin failed, cause by: " << error.mDescription << std::endl;
        return;
    }

    mClient->getChatroomManager().removeChatroomAdmin(chatRoomPtr->chatroomId(), TestAccounts[1], error);
    if(error.mErrorCode != EMError::EM_NO_ERROR) {
        std::cerr << "removeChatroomAdmin failed, cause by: " << error.mDescription << std::endl;
        return;
    }


//    mClient->getChatroomManager().transferChatroomOwner(chatRoomPtr->chatroomId(), "ceshib", error);
//
//    if(error.mErrorCode != EMError::EM_NO_ERROR) {
//        std::cout << "transferChatroomOwner failed, cause by: " << error.mDescription << std::endl;
//        return;
//    }

    mClient->getChatroomManager().fetchChatroomAnnouncement(chatRoomPtr->chatroomId(), error);
    if(error.mErrorCode != EMError::EM_NO_ERROR) {
        std::cerr << "fetchChatroomAnnouncement failed, cause by: " << error.mDescription << std::endl;
        return;
    }

    mClient->getChatroomManager().updateChatroomAnnouncement(chatRoomPtr->chatroomId(), "newAnnouncement", error);
    if(error.mErrorCode != EMError::EM_NO_ERROR) {
        std::cerr << "updateChatroomAnnouncement failed, cause by: " << error.mDescription << std::endl;
        return;
    }

    mClient->getChatroomManager().changeChatroomSubject(chatRoomPtr->chatroomId(), "newSubject", error);
    if(error.mErrorCode != EMError::EM_NO_ERROR) {
        std::cerr << "changeChatroomSubject failed, cause by: " << error.mDescription << std::endl;
        return;
    }

    mClient->getChatroomManager().changeChatroomDescription(chatRoomPtr->chatroomId(), "newDescription", error);
    if(error.mErrorCode != EMError::EM_NO_ERROR) {
        std::cerr << "changeChatroomDescription failed, cause by: " << error.mDescription << std::endl;
        return;
    }

    mClient->getChatroomManager().destroyChatroom(chatRoomPtr->chatroomId(), error);
    if(error.mErrorCode != EMError::EM_NO_ERROR) {
        std::cerr << "destroyChatroom failed, cause by: " << error.mDescription << std::endl;
        return;
    }
    std::cout << "destroyChatroom success" << std::endl;



    mClient->getChatroomManager().removeListener(listener);

    delete listener;

    mClient->logout();

}
