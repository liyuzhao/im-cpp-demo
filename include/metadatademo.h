//
// Created by liyuzhao on 12/15/22.
//

#ifndef DEMO_METADATADEMO_H
#define DEMO_METADATADEMO_H
#include "baseconfig.h"

class MetaDataDemo : public BaseConfig{

public:
    MetaDataDemo();

    void testMetaData();

};


#endif //DEMO_METADATADEMO_H
