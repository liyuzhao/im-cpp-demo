#ifndef SIMPLE_DEMO_H
#define SIMPLE_DEMO_H

#include "baseconfig.h"
/**
 * 简单的Demo测试，主要为：配置私有部署环境、向某人发送一条消息、怎样监听收到的消息
 */
class SimpleDemo : public BaseConfig{

public:
    SimpleDemo() : BaseConfig() {}

public:
    void testSendMessage();
};


#endif //SIMPLE_DEMO_H
