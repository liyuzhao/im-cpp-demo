//
// Created by liyuzhao on 11/9/22.
//

#ifndef CHATROOMDEMO_H
#define CHATROOMDEMO_H
#include "baseconfig.h"

class ChatRoomDemo : public BaseConfig{

public:
    ChatRoomDemo();

    void testChatRoom();

};


#endif //CHATROOMDEMO_H
