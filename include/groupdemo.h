#ifndef GROUP_DEMO_H
#define GROUP_DEMO_H
#include "baseconfig.h"

class GroupDemo : public BaseConfig {

public:
    GroupDemo();
    void testGroup();
};


#endif //GROUP_DEMO_H
