//
// Created by liyuzhao on 2023/9/11.
//

#ifndef DEMO_BASECONFIG_H
#define DEMO_BASECONFIG_H
#include <emclient.h>
#include <vector>

class BaseConfig {
public:
    std::vector<std::string> TestAccounts;

public:
    BaseConfig();
    virtual void initPrivate();
    virtual void createTestAccount();
    easemob::EMClient* mClient;

};


#endif //DEMO_BASECONFIG_H
