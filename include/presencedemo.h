//
// Created by liyuzhao on 12/20/22.
//

#ifndef PRESENCEDEMO_H
#define PRESENCEDEMO_H
#include "baseconfig.h"

class PresenceDemo : public BaseConfig {

public:
    PresenceDemo();
    void testPresence();
};


#endif //PRESENCEDEMO_H
