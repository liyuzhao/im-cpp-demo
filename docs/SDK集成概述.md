# C++ SDK 文档介绍

### 其他文档介绍

[产品概述](http://docs-im-beta.easemob.com/product/introduction.html)

[快速开始](http://docs-im-beta.easemob.com/product/enable_and_configure_IM.html)

### 注册appkey

使用环信C++SDK前，需要先申请一个Appkey，如您为公有云客户，[点击官网注册](https://console.easemob.com/user/register)。

如您为私有云部署用户，请询问部署人员或商务人员。

## 项目设置

配置环信SDK到您的IDE中，[在Windows中使用时](windows.h)。

## 添加命名空间

```
using namespace easemob;
```

## 初始化

初始化为第一步，先设置工作路径及应用的Appkey。

```
// 配置参数：1. 资源路径(下载的文件等)  2. 工作路径(日志、配置信息等)  3. appkey
easemob::EMChatConfigsPtr configs(new easemob::EMChatConfigs("./resourcePath", "./workPath", "100191010152293#mpapp5"));
easemob::EMChatConfigsPtr configPtr = easemob::EMChatConfigsPtr(configs);

// 配置Resources: 移动端、Windows、Mac、Linux等
configs->setOs(EMChatConfigs::OS_MSWIN);
configs->setClientResource("win"); // resource 名称
configPtr->setEnableConsoleLog(true); // 打印控制台日志
```

## 私有环境配置

私有部署的环境，需向运维人员或商务人员获取到服务器的IP地址、端口号及Appkey。

```
	... 初始化部分代码 ...
	
	// 设置私有部署环境
    auto priConfig = configPtr->privateConfigs();
    priConfig->enableDnsConfig(false);
    priConfig->chatServer() = "47.93.40.123";
    priConfig->chatPort() = 6717;
    priConfig->restServer() = "47.93.40.123:12001";

```

## 创建账号

在 **开放注册** 模式下, 是可以调用SDK的方法注册IM用户的。


```
... 初始化部分代码 ...

easemob::EMClient* mClient = easemob::EMClient::create(configPtr);

auto errorPtr = mClient->createAccount("username", "password");
if(errorPtr->mErrorCode == EMError::EM_NO_ERROR) {
    std::cout << "create account success！" << std::endl;
} else {
    std::cout << "create account failed, cause by：" << errorPtr->mDescription << std::endl;
}
```

## 登录账号

```
... 初始化部分代码 ...

easemob::EMClient* mClient = easemob::EMClient::create(configPtr);
easemob::EMErrorPtr retPtr = mClient->login("ceshia", "123456");
if (retPtr->mErrorCode == EMError::EM_NO_ERROR) {
    std::cout << "login success!" << std::endl;
}
else {
    std::cout << "login failed!" << retPtr->mErrorCode << std::endl;
}

```

## 登出账号

```
... 初始化部分代码 ...

easemob::EMClient* mClient = easemob::EMClient::create(configPtr);

... 其他逻辑代码 ...

mClient->logout();

delete mClient;
```

## 发送一条文本消息

```
......
......
// 创建一个文本类型的消息体
EMMessageBodyPtr body = EMTextMessageBodyPtr(new EMTextMessageBody("hello world with txt type"));

// 选择是单聊还是群聊        
EMMessage::EMChatType chatType = EMMessage::SINGLE;

//创建待发消息， 参数分别为：from, to, body, chatType
EMMessagePtr msg = EMMessage::createSendMessage(mClient->getLoginInfo().loginUser(), "ceshib", body, chatType);

// 添加消息回调
EMCallbackPtr msgCallback(new EMCallback(handle,
                                                 [=](void)->bool {
                                                     cout << "send message sucess!" << endl;
                                                     return true;
                                                 },
                                                 [=](const easemob::EMErrorPtr ePtr)->bool
                                                 {
                                                     cout << "send message failed!" << ePtr->mErrorCode << endl;
                                                     return false;
                                                 },
                                                 [](int progress) {}));

msg->setCallback(msgCallback);
mClient->getChatManager().sendMessage(msg);
......
......
```

## 接收消息

接收消息需要对象继承 `EMChatManagerListener`, 并实现相关的回调函数，同时将对象加入到监听列表中。

```
class ChatListener : public EMChatManagerListener {
public:
    ChatListener()
    {
    }

    virtual void onReceiveMessages(const EMMessageList& messages);

private:
    EMCallbackObserverHandle m_coh;
    void onTextMessage(const EMMessagePtr msg, const EMMessageBodyPtr _body, string sChatType);
    void onFileMessage(const EMMessagePtr msg, const EMMessageBodyPtr _body, string sChatType);
    void onImageMessage(const EMMessagePtr msg, const EMMessageBodyPtr _body, string sChatType);
    void onVoiceMessage(const EMMessagePtr msg, const EMMessageBodyPtr _body, string sChatType);
    void onVideoMessage(const EMMessagePtr msg, const EMMessageBodyPtr _body, string sChatType);
    void onLocationMessage(const EMMessagePtr msg, const EMMessageBodyPtr _body, string sChatType);
};

void ChatListener::onReceiveMessages(const EMMessageList& messages)
{

    for (EMMessagePtr msg : messages)
    {
        EMMessage::EMChatType type = msg->chatType();
        string sChatType = "chat";
        if (type == EMMessage::GROUP)
        {
            sChatType = "groupchat";
        }
        else if (type == EMMessage::CHATROOM)
        {
            sChatType = "chatroom";
        }
        const vector<EMMessageBodyPtr>& bodies = msg->bodies();
        const EMMessageBodyPtr _body = bodies[0];
        switch (_body->type())
        {
            case EMMessageBody::TEXT:
            {
                onTextMessage(msg, _body, sChatType);
                break;
            }
            case EMMessageBody::FILE:
            {
                onFileMessage(msg, _body, sChatType);
                break;
            }
            case EMMessageBody::IMAGE:
            {
                onImageMessage(msg, _body, sChatType);
                break;
            }
            case EMMessageBody::VOICE:
            {
                onVoiceMessage(msg, _body, sChatType);
                break;
            }
            case EMMessageBody::COMMAND:
                break;
            case EMMessageBody::VIDEO:
            {
                onVideoMessage(msg, _body, sChatType);
                break;
            }
            case EMMessageBody::LOCATION:
            {
                onLocationMessage(msg, _body, sChatType);
                break;
            }
        }
    }
}

void ChatListener::onTextMessage(const EMMessagePtr msg, const EMMessageBodyPtr _body, string sChatType)
{
    cout << "received onTextMessage" << endl;

    EMTextMessageBodyPtr body = std::dynamic_pointer_cast<EMTextMessageBody, EMMessageBody>(_body);
    std::stringstream stream;
    stream << "Demo.conn.onTextMessage('{id: \"";
    stream << msg->msgId();
    stream << "\",type : \"";
    stream << sChatType;
    stream << "\", from : \"";
    stream << msg->from();
    stream << "\",to : \"";
    stream << msg->to();
    stream << "\",data : \"";
    stream << body->text();
    stream << "\",ext : \"\"}');";

    cout << stream.str() << endl;

}
void ChatListener::onFileMessage(const EMMessagePtr msg, const EMMessageBodyPtr _body, string sChatType)
{}

void ChatListener::onImageMessage(const EMMessagePtr msg, const EMMessageBodyPtr _body, string sChatType)
{}
void ChatListener::onVoiceMessage(const EMMessagePtr msg, const EMMessageBodyPtr _body, string sChatType)
{}
void ChatListener::onVideoMessage(const EMMessagePtr msg, const EMMessageBodyPtr _body, string sChatType)
{}
void ChatListener::onLocationMessage(const EMMessagePtr msg, const EMMessageBodyPtr _body, string sChatType)
{}
```

并将对象实例加入到监听列表中

```
ChatListener* mChatListener = new ChatListener();
mClient->getChatManager().addListener(mChatListener);
......
delete mChatListener;
```

## 监听连接情况

在某些情况下，需要显示”连不到服务器“或者”当前无网络“等提示, 也可能需要监听”账号在其他地方登录“，则需要添加此监听

```
class ConnectionListener : public easemob::EMConnectionListener {

public:
    ConnectionListener()
    {
    }
    virtual void onConnect(const std::string& info = "") { cout << "---> connected:" << info << endl; }
    virtual void onDisconnect(EMErrorPtr error) {
        if (error->mErrorCode == EMError::USER_LOGIN_ANOTHER_DEVICE) {
            cout << "---> was kicked out" << endl;
        }
    }
};

```

并将实力对象添加到监听列表中

```
ConnectionListener* listener = new ConnectionListener();
mClient->addConnectionListener(listener);

... 其他业务逻辑 ...

delete listener;

```

### 被动退出登录

对于 `onDisconnect` 通知，这些 `errorCode` 需要用户关注，收到如下通知，建议App返回登录界面。

- `USER_LOGIN_ANOTHER_DEVICE=206`: 用户已经在其他设备登录
- `USER_REMOVED=207`: 用户账户已经被移除
- `USER_BIND_ANOTHER_DEVICE=213`: 用户已经绑定其他设备
- `SERVER_SERVING_DISABLED=305`: 服务器服务停止
- `USER_LOGIN_TOO_MANY_DEVICES=214`: 用户登录设备超出数量限制
- `USER_KICKED_BY_CHANGE_PASSWORD=216`: 由于密码变更被踢下线
- `USER_KICKED_BY_OTHER_DEVICE=217`: 由于其他设备登录被踢下线
- `USER_DEVICE_CHANGED=220`: 和上次设备不同导致下线

## 日志文件

如开启日志调试模式，会通过控制台输出日志，

```
configPtr->setEnableConsoleLog(true); // 打印控制台日志
```

当遇到问题，可导出SDK的日志，日志文件的位置为(workPath见 **初始化** 部分)：

```
{workPath}/easemobLog/easemob.log
```