# 聊天室概述

## 功能描述

聊天室是支持多人加入的类似 Twitch 的组织。聊天室中的成员没有固定关系，用户离线后，超过 5 分钟会自动退出聊天室。聊天室成员在离线后，不会收到推送消息。聊天室可以应用于直播、消息广播等。

聊天室的使用限制视不同套餐版本而定，请参见 [使用限制](http://docs-im-beta.easemob.com/product/limitation.html)。

本文以及接下来几篇主要介绍聊天室管理功能，如需查看消息相关内容，参见 [消息管理](消息管理.md)。

## 群组与聊天室的区别

群组和聊天室均为支持多人沟通的即时通讯系统。两者的区别在于，群组中的成员会有固定的强的关系，成员加入后会长时间的在群组中。聊天室中的成员没有固定关系，类似与一个开放的空间，用户可以自由加入，离开即退出聊天室。

### 群组与聊天室的功能对比

见 [群组概述](群组概述.md)。

## 功能列表

### 聊天室角色介绍


|聊天室成员角色类型|描述|管理权限|
|:--:|:--:|:--:|
|普通成员|不具备管理权限的聊天室成员。|	普通成员可以修改自己的聊天室资料。|
|聊天室管理员|由聊天室所有者授权，协助进行管理，拥有一定的管理权限。|管理员可以对普通成员进行管理。|
|聊天室所有者|一般是聊天室的创建者，在聊天室中拥有最高的权限。|聊天室所有者可以指定管理员，解散聊天室，更改聊天室名称描述等信息，以及对聊天室成员进行管理。|


### 聊天室操作

|功能|描述|
|:--|:--|
|创建聊天室|只有被赋予 **超级管理员** 权限的用户有权限创建聊天室。聊天室成员数会受到版本指定聊天室最大成员数的限制。|
|加入聊天室|没有被加入黑名单的所有 app 用户可自由加入聊天室。|
|离开聊天室|所有聊天室成员都可以自由退出聊天室；也可能被动离开聊天室，原因分为：被管理员移出聊天室、聊天室解散和用户账号离线。|
|销毁聊天室|需要聊天室所有者权限。|
|获取聊天室列表|所有 app 用户有权限获取聊天室列表。|
|获取聊天室详情|所有聊天室成员有权限获取聊天室详情。|
|修改聊天室名称|需要聊天室所有者权限。|
|聊天室公告|仅聊天室所有者有权限编辑公告、删除公告。公告更新会通过监听同步给所有成员。|

### 聊天室成员管理

|功能|描述|
|:--|:--|
|将聊天室成员加入禁言名单|需要聊天室所有者或管理员权限，可以对单个聊天室成员进行禁言。|
|聊天室全员禁言|需要聊天室所有者或管理员权限。全员禁言时，默认聊天室所有者和管理员不禁言。|
|白名单管理|需要聊天室所有者或管理员权限。全员禁言时，白名单的成员可以发消息。|
|聊天室管理员|仅聊天室所有者可以对成员指定或移除管理员权限。|
|黑名单管理	|需要聊天室所有者或管理员权限，被加入黑名单的成员会被移出聊天室。黑名单中的成员需要聊天室所有者主动从黑名单移除后才能再次加入聊天室。|

# 创建和管理聊天室以及监听器介绍

聊天室是支持多人沟通的即时通讯系统。聊天室中的成员没有固定关系，用户离线后，超过 5 分钟会自动退出聊天室。聊天室成员在离线后，不会收到推送消息。聊天室可以应用于直播、消息广播等。

本文介绍如何使用环信即时通讯 IM SDK 在实时互动 app 中创建和管理聊天室，并实现聊天室的相关功能。

消息相关内容见 [消息管理](消息管理.md)。

## 前提条件

开始前，请确保满足一下条件：

- 完成 SDK 初始化，详见 [快速开始](SDK集成概述.md)。
- 了解环信即时通讯 IM 的使用限制，详见 [使用限制](http://docs-im-beta.easemob.com/product/limitation.html)，私有部署用户需咨询运维或商务人员。
- 了解环信即时通讯 IM 不同版本的聊天室相关数量限制，详见 [环信即时通讯 IM 价格](https://www.easemob.com/pricing/im)
- 只有超级管理员才有创建聊天室的权限，因此你还需要确保已调用 RESTful API 添加了超级管理员，详见 [添加聊天室超级管理员](#添加聊天室超级管理员)；
- 聊天室创建者和管理员的数量之和不能超过 100，即管理员最多可添加 99 个。

## 实现方法

本节介绍如何使用环信即时通讯 IM SDK 提供的 API 实现上述功能。

### 创建聊天室

仅 [<font color="red">**超级管理员**</font>](http://docs-im-beta.easemob.com/document/server-side/chatroom.html#%E7%AE%A1%E7%90%86%E8%B6%85%E7%BA%A7%E7%AE%A1%E7%90%86%E5%91%98) 可以调用 `createChatroom` 方法创建聊天室，并设置聊天室的名称、描述、最大成员数等信息。成功创建聊天室后，该超级管理员为该聊天室的所有者。

你也可以直接调用 REST API [从服务端创建聊天室](http://docs-im-beta.easemob.com/document/server-side/chatroom.html#%E5%88%9B%E5%BB%BA%E8%81%8A%E5%A4%A9%E5%AE%A4)。

示例代码如下：

```
	EMError error;
    int maxUserCount = 200;
    bool inviteNeedConfirm = false;
    auto chatRoomPtr = mClient->getChatroomManager().createChatroom("chatRoomName", "chatRoomDesc", "welcomeMessage", EMMucSetting(EMMucSetting::EMMucStyle::PUBLIC_JOIN_OPEN, maxUserCount, inviteNeedConfirm), {"b2bua001"}, error);
```

### 加入聊天室

用户申请加入聊天室的步骤如下：

调用 `fetchAllChatrooms` 方法从服务器获取聊天室列表，查询到想要加入的聊天室 ID。
调用 `joinChatroom` 方法传入聊天室 ID，申请加入对应聊天室。新成员加入聊天室时，其他成员收到 `onMemberJoinedChatroom` 回调。

示例代码如下：

```
auto chatRoomList = mClient->getChatroomManager().fetchAllChatrooms(error);

mClient->getChatroomManager().joinChatroom(chatRoomPtr->chatroomId(), error);
```

### 获取聊天室详情

聊天室成员均可调用 `fetchChatroomSpecification` 获取聊天室详情，包括聊天室 ID、聊天室名称，聊天室描述、聊天室公告、管理员列表、最大成员数、聊天室所有者、是否全员禁言以及聊天室权限类型。成员列表、黑名单列表、禁言列表需单独调用接口获取。

示例代码如下：

```
auto chatRoomS = mClient->getChatroomManager().fetchChatroomSpecification(chatRoomPtr->chatroomId(), error);
```

### 退出聊天室

聊天室所有成员均可以调用 `leaveChatroom` 方法退出当前聊天室。成员退出聊天室时，其他成员收到 `onMemberLeftChatroom` 回调。

示例代码如下：

```
mClient->getChatroomManager().leaveChatroom(chatRoomPtr->chatroomId(), error);
```

退出聊天室时，SDK 默认删除该聊天室的所有本地消息，若要保留这些消息，可在 SDK 初始化时将 `EMChatConfigs#setDeleteMessageAsExitChatRoom` 设置为 false。

示例代码如下：

```
configPtr->setDeleteMessageAsExitChatRoom(false);
```

与群主无法退出群组不同，聊天室所有者可以离开聊天室，例如所有者从服务器下线则 5 分钟后自动离开聊天室。如果所有者重新进入聊天室仍是该聊天室的所有者。

### 解散聊天室

仅聊天室所有者可以调用 `destroyChatroom` 方法解散聊天室。聊天室解散时，其他聊天室成员收到 `onLeaveChatroom` 回调并被踢出聊天室。

示例代码如下：

```
mClient->getChatroomManager().destroyChatroom(chatRoomPtr->chatroomId(), error);
```

### 监听聊天室事件

`EMChatroomManagerListener` 类中提供了聊天室事件的监听接口。你可以通过注册聊天室监听器，获取聊天室事件，并作出相应处理。如不再使用该监听，需要移除，防止出现内存泄露。

示例代码如下：

```
// 实现监听器以及定义监听器对象
class ChatRoomListener : public EMChatroomManagerListener {
public:
    ChatRoomListener(){}
    void onLeaveChatroom(const EMChatroomPtr chatroom, EMMuc::EMMucLeaveReason reason) {}
     ......

};
// 注册聊天室回调
ChatRoomListener * listener = new ChatRoomListener();
mClient->getChatroomManager().addListener(listener);

// 移除聊天室回调
mClient->getChatroomManager().removeListener(listener);

```

具体可以添加的回调事件如下：

```
class ChatRoomListener : public EMChatroomManagerListener {
public:
    ChatRoomListener(){}
    void onLeaveChatroom(const EMChatroomPtr chatroom, EMMuc::EMMucLeaveReason reason) {}
    void onMemberJoinedChatroom(const EMChatroomPtr chatroom, const std::string &member) {};
    void onMemberLeftChatroom(const EMChatroomPtr chatroom, const std::string &member) {};
    void onAddMutesFromChatroom(const EMChatroomPtr chatroom, const std::vector<std::string> &mutes, int64_t muteExpire) {};
    void onRemoveMutesFromChatroom(const EMChatroomPtr chatroom, const std::vector<std::string> &mutes) {};
    void onAddWhiteListMembersFromChatroom(const easemob::EMChatroomPtr chatroom, const std::vector<std::string> &members) {};
    void onRemoveWhiteListMembersFromChatroom(const easemob::EMChatroomPtr chatroom, const std::vector<std::string> &members) {};
    void onAllMemberMuteChangedFromChatroom(const easemob::EMChatroomPtr chatroom, bool isAllMuted) {};
    void onAddAdminFromChatroom(const EMChatroomPtr chatroom, const std::string &admin) {};
    void onRemoveAdminFromChatroom(const EMChatroomPtr chatroom, const std::string &admin) {};
    void onAssignOwnerFromChatroom(const EMChatroomPtr chatroom, const std::string &newOwner, const std::string &oldOwner) {};
    void onUpdateAnnouncementFromChatroom(const EMChatroomPtr chatroom, const std::string &announcement) {};

};
```

# 管理聊天室成员

聊天室是支持多人沟通的即时通讯系统。本文介绍如何使用环信即时通讯 IM SDK 在实时互动 app 中管理聊天室成员，并实现聊天室的相关功能。


## 前提条件

开始前，请确保满足一下条件：

- 完成 SDK 初始化，详见 [快速开始](SDK集成概述.md)。
- 了解环信即时通讯 IM 的使用限制，详见 [使用限制](http://docs-im-beta.easemob.com/product/limitation.html)，私有部署用户需咨询运维或商务人员。
- 了解环信即时通讯 IM 不同版本的聊天室相关数量限制，详见 [环信即时通讯 IM 价格](https://www.easemob.com/pricing/im)

## 实现方法
本节介绍如何使用环信即时通讯 IM SDK 提供的 API 实现上述功能。

### 获取聊天室成员列表

所有聊天室成员均可调用 `fetchChatroomMembers` 方法获取当前聊天室的成员列表。

示例代码如下：

```
auto cursorRet = mClient->getChatroomManager().fetchChatroomMembers(chatRoomPtr->chatroomId(), cursor, pageSize, error);

for(auto & s : cursorRet.result()) {
    std::cout << "member =>" << s << std::endl;
}

```

### 将成员移出聊天室

仅聊天室所有者和管理员可调用 `removeChatroomMembers` 方法将指定成员移出聊天室。

被移出后，该成员收到 `onLeaveChatroom` 回调，其他成员收到 OnMemberExitedFromRoom 回调。

被移出的成员可以重新进入聊天室。

示例代码如下：

```
mClient->getChatroomManager().removeChatroomMembers(chatRoomPtr->chatroomId(), {"b2bua001"}, error);
```

### 管理聊天室黑名单

**将成员加入聊天室黑名单**

仅聊天室所有者和管理员可调用 `blockChatroomMembers` 方法将指定成员添加至黑名单。被加入黑名单后，该成员收到 `onLeaveChatroom` 回调，其他成员收到 `onMemberLeftChatroom` 回调。被加入黑名单后，该成员无法再收发聊天室消息并被移出聊天室，黑名单中的成员如想再次加入聊天室，聊天室所有者或管理员必须先将其移出黑名单列表。

示例代码如下：

```
mClient->getChatroomManager().blockChatroomMembers(chatRoomPtr->chatroomId(), {"ceshia"}, error);
```

**将成员移出聊天室黑名单**

仅聊天室所有者和管理员可以调用 `unblockChatroomMembers` 方法将成员移出聊天室黑名单。

示例代码如下：

```
mClient->getChatroomManager().unblockChatroomMembers(chatRoomPtr->chatroomId(), {"ceshia"}, error);
```

**获取聊天室黑名单列表**

仅聊天室所有者和管理员可以调用 `fetchChatroomBans` 方法获取当前聊天室黑名单。

示例代码如下：

```
mClient->getChatroomManager().fetchChatroomBans(chatRoomPtr->chatroomId(), 0, pageSize, error);
```

### 管理聊天室禁言列表

**添加成员至聊天室禁言列表**

仅聊天室所有者和管理员可以调用 `muteChatroomMembers` 方法将指定成员添加至聊天室禁言列表。被禁言后，该成员和其他未操作的聊天室管理员或聊天室所有者收到 `onAddMutesFromChatroom` 回调。

>注意: 聊天室所有者可禁言聊天室所有成员，聊天室管理员可禁言聊天室普通成员。

示例代码如下

```
mClient->getChatroomManager().muteChatroomMembers(chatRoomPtr->chatroomId(), {"ceshic"}, 60 * 1000, error);
```


**将成员移出聊天室禁言列表**

仅聊天室所有者和管理员可以调用 `unmuteChatroomMembers` 方法将成员移出聊天室禁言列表。被解除禁言后，其他成员收到 `onRemoveMutesFromChatroom` 回调。

>注意: 聊天室所有者可对聊天室所有成员解除禁言，聊天室管理员可对聊天室普通成员解除禁言。

示例代码如下：

```
mClient->getChatroomManager().unmuteChatroomMembers(chatRoomPtr->chatroomId(), {"ceshic"}, error);
```

**获取聊天室禁言列表**

仅聊天室所有者和管理员可调用 `fetchChatroomMutes` 方法获取当前聊天室的禁言列表。

示例代码如下：

```
mClient->getChatroomManager().fetchChatroomMutes(chatRoomPtr->chatroomId(), 0, pageSize, error);
```

### 管理聊天室所有者和管理员

**变更聊天室所有者**

仅聊天室所有者可以调用 `transferChatroomOwner` 方法将权限移交给聊天室中指定成员。成功移交后，原聊天室所有者变为聊天室成员，新的聊天室所有者和聊天室管理员收到 `onAssignOwnerFromChatroom` 回调。

示例代码如下：

```
mClient->getChatroomManager().transferChatroomOwner(chatRoomPtr->chatroomId(), "ceshib", error);
```

**添加聊天室管理员**

仅聊天室所有者可以调用 `addChatroomAdmin` 方法添加聊天室管理员。成功添加后，新管理员及其他管理员收到 `onAddAdminFromChatroom` 回调。

示例代码如下：

```
mClient->getChatroomManager().addChatroomAdmin(chatRoomPtr->chatroomId(), "ceshib", error);
```

**移除聊天室管理员**

仅聊天室所有者可以调用 `removeChatroomAdmin` 方法移除聊天室管理员。成功移除后，被移除的管理员及其他管理员收到 `onRemoveAdminFromChatroom` 回调。

示例代码如下：

```
mClient->getChatroomManager().removeChatroomAdmin(chatRoomPtr->chatroomId(), "ceshib", error);
```

监听聊天室事件
详见 **监听聊天室事件**。

# 管理聊天室属性

聊天室是支持多人沟通的即时通讯系统。聊天室属性可分为聊天室名称、描述和公告等基本属性和自定义属性（key-value）。若聊天室基本属性不满足业务要求，用户可增加自定义属性并同步给所有成员。利用自定义属性可以存储直播聊天室的类型、狼人杀等游戏中的角色信息和游戏状态以及实现语聊房的麦位管理和同步等。聊天室自定义属性以键值对（key-value）形式存储，属性信息变更会实时同步给聊天室成员。

本文介绍如何管理聊天室属性信息。

## 前提条件

开始前，请确保满足一下条件：

- 完成 SDK 初始化，详见 [快速开始](SDK集成概述.md)。
- 了解环信即时通讯 IM 的使用限制，详见 [使用限制](http://docs-im-beta.easemob.com/product/limitation.html)，私有部署用户需咨询运维或商务人员。
- 了解环信即时通讯 IM 不同版本的聊天室相关数量限制，详见 [环信即时通讯 IM 价格](https://www.easemob.com/pricing/im)

## 实现方法

### 管理聊天室基本属性

对于聊天室名称和描述，你可以调用 `fetchChatroomSpecification` 获取聊天室详情时查看。

**获取聊天室公告**

聊天室所有成员均可调用 `fetchChatroomAnnouncement` 方法获取聊天室公告。

示例代码如下：

```
mClient->getChatroomManager().fetchChatroomAnnouncement(chatRoomPtr->chatroomId(), error);
```

**更新聊天室公告**

仅聊天室所有者和聊天室管理员可以调用 `updateChatroomAnnouncement` 方法设置和更新聊天室公告，聊天室公告的长度限制为 512 个字符。公告更新后，其他聊天室成员收到 `onUpdateAnnouncementFromChatroom` 回调。

示例代码如下：

```
mClient->getChatroomManager().updateChatroomAnnouncement(chatRoomPtr->chatroomId(), "newAnnouncement", error);
```

**修改聊天室名称**

仅聊天室所有者和聊天室管理员可以调用 `changeChatroomSubject` 方法设置和修改聊天室名称，聊天室名称的长度限制为 128 个字符。

示例代码如下：

```
mClient->getChatroomManager().changeChatroomSubject(chatRoomPtr->chatroomId(), "newSubject", error);
```

**修改聊天室描述**

仅聊天室所有者和聊天室管理员可以调用 `changeChatroomDescription` 方法设置和修改聊天室描述，聊天室描述的长度限制为 512 个字符。

示例代码如下：

```
mClient->getChatroomManager().changeChatroomDescription(chatRoomPtr->chatroomId(), "newDescription", error);
```